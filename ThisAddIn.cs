﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Outlook;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Redemption;

namespace BugReproduceOutlookAddIn
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new Ribbon();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see https://go.microsoft.com/fwlink/?LinkId=506785
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }

    [ComVisible(true)]
    public class Ribbon : Office.IRibbonExtensibility
    {
        public Ribbon(){}

        #region IRibbonExtensibility Members
        public string GetCustomUI(string ribbonID)
        {
            if ("Microsoft.Outlook.Explorer".Equals(ribbonID))
            {
                string customUI = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" +
                    " <customUI xmlns=\"http://schemas.microsoft.com/office/2009/07/customui\" \r\n" +
                    "  onLoad=\"Ribbon_Load\" xmlns:x=\"MySpace.Outlook\"> \r\n" +
                    "   <contextMenus>\r\n" +
                    "    <contextMenu idMso=\"ContextMenuMailItem\">\r\n" +
                    "      <button\r\n id=\"MyContextMenuDecryptItem\" " +
                    "            label=\"Decrypt message\" \r\n" +
                    "            imageMso=\"EncryptMessage\" \r\n" +
                    "            onAction=\"DecryptMessageButton_Click\"" +
                    "       />\r\n" +
                    "    </contextMenu>\r\n" +
                    "  </contextMenus> \r\n" +
                    "</customUI> ";

                return customUI;
            }
            else
            {
                return String.Empty;
            }
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, select the Ribbon XML item in Solution Explorer and then press F1

        public void DecryptMessageButton_Click(IRibbonControl control)
        {
            try
            {
                if (!(control.Context is Selection)) return;
                Selection selection = control.Context;
                if (selection.Count != 1) return;

                // Load Redemption through registry                
                Type rdoSessType = Type.GetTypeFromProgID("Redemption.RDOSession");
                dynamic dynObj = Activator.CreateInstance(rdoSessType);
                if (!(dynObj is RDOSession)) { return; }
                RDOSession rSession = dynObj as RDOSession;
                rSession.MAPIOBJECT = selection.Session.MAPIOBJECT;

                RDOFolder IPMRoot = rSession.Stores.DefaultStore.IPMRootFolder;
                RDOFolder unsecuredFolder = IPMRoot.Folders.OpenOrAdd("Test decrypted", rdoDefaultFolders.olFolderDrafts);

                var item = selection[1];
                if (!(item is MailItem)) return;
                MailItem oMail = item as MailItem;
                RDOMail decMail = null;

                RDOMail rMail = rSession.GetMessageFromID(oMail.EntryID);
                if (!(rMail is RDOEncryptedMessage)) return;
                RDOEncryptedMessage encMail = rMail as RDOEncryptedMessage;
                decMail = encMail.GetDecryptedMessage();
                decMail.Move(unsecuredFolder);
                decMail.Save();
                
                MessageBox.Show("The message has been copied to " +unsecuredFolder.Name + " folder", "Bug Reproduce addin");
            }
            catch (System.Exception e)
            {
                MessageBox.Show(e.Message, "Bug Reproduce addin");
            }
        }

        #endregion

        #region Helpers

        #endregion
    }

}
